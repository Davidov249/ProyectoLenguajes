Para la utilizaci�n del proyecto, se corre el programa jflex y se escoge el archivo MinPHP.flex, esto generar� un archivo
llamado "MiniPHP.java" dicho archivo tendr� que ser compilado con el comando: "javac MiniPHP.java" una vez compilado
se podr� correr el programa usando el comando "java MiniPHP [Nombre del archivo]" Se le pedir� que ingrese el nombre del 
archivo nuevamente.

El programa analizar� el archivo ingresado y crear� un archivo llamado [NombredelArchivo].out en el cual se podr� observar
el codigo php con su formato adecuado (lo que es en mayusculas en mayusculas y lo que es en minusculas en minusculas)
o en el caso de que no pertenezca al lenguaje se sacar� el mismo archivo notificando donde se encuentra cada error que se
encontr�.