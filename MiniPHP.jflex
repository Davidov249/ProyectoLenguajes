import java.lang.Object;
import java.io.PrintWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.lang.System;
import java.io.Console;
import java.io.InputStream;
import java.util.Scanner;

%%
%class MiniPHP
%unicode
%public
%standalone
%line
%column
%char
ASCII = \\p{ASCII}*
Espacio = [\t\n]+
EtiquetaInicio = \<\?([Pp][Hh][Pp])
OperadoresA = [\+\*-/%(\*\*)]
OperadoresL = [([aA][nN][dD])([oO][rR])([xX][oO][rR])!(\&\&)(\|\|)]
Variables = \$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*
VariablesVariables = \$\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*
Bool = [([Tt][Rr][Ur][Ee])([Ff][Aa][Ll][Ss][Ee])]
Enteros = [\+-]?[([([1-9][0-9]*)0])(0[xX][0-9a-fA-F]+)(0[0-7]+)(0[bB][01]+)]
Reales = [\+-]?([([1-9][0-9]*)|0]).[0-9]*
Cadenas = \'(\s|\S)*\'
CadenasCDoble = \"(\s|\S)*\"
Global = \$[Gg][Ll][Oo][Bb][Aa][Ll][Ss]
Server = \$_[Ss][Ee][Rr][Vv][Ee][Rr][Ss]
GET = \$_[Gg][Ee][Tt]
POST = \$_[Pp][Oo][Ss][Tt]
FILES = \$_[Ff][Ii][Ll][Ee][Ss]
COOKIE = \$_[Cc][Oo][Oo][Kk][Ii][Ee]
SESSION = \$_[Ss][Ee][Ss][Ss][Ii][Oo][Nn]
REQUEST = \$_[Rr][Ee][Qq][Uu][Ee][Ss][Tt]
ENV = \$_[Ee][Nn][Vv]
COOKIE = \$_[Cc][Oo][Oo][Kk][Ii][Ee]
MsjError = \$[Pp][Hh][Pp]_[Ee][Rr][Rr][Oo][Rr][Mm][Ss][Gg]
DatosPost = \$[Hh][Tt][Tt][Pp]_[Rr][Aa][Ww]_[Pp][Oo][Ss][Tt]_[Dd][Aa][Tt][Aa]
EncabezadosHTTP = \$[Hh][Tt][Tt][Pp]_[Rr][Ee][Ss][Pp][Oo][Nn][Ss][Ee]_[Hh][Ee][Aa][Dd][Ee][Rr]
NumArgScript = \$[Aa][Rr][Gg][Cc]
ArrayArgScript = \$[Aa][Rr][Gg][Vv]
Constatntes = [a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*
Identificador = [Tt]_[a-zA-Z_]w*
Comentario = \/\/(\w|\s)*
ComentarioMultiliena = \/\*(\s|\S)*\*\/
ComentarioConsola = #([ |t|\S])*
PalabrasR = [(__[Hh][Aa][Ll][Tt]_[Cc][Oo][Mm][Pp][Ii][Ll][Ee][Rr]\(\))([Aa][Bb][Ss][Tt][Rr][Aa][Cc][Tt])([Aa][Nn][Dd])([Aa][Rr][Rr][Aa][Yy]\(\))([Aa][Ss])([Cc][Ll][Oo][Nn][Ee])([Cc][Oo][Nn][Ss][Tt])([Bb][Rr][Ee][Aa][Kk])([Cc][Aa][Ll][Ll][Aa][Bb][Ll][Ee])([Cc][Aa][Ss][Ee])([Cc][Aa][Tt][Cc][Hh])([Cc][Ll][Aa][Ss][Ss])([Cc][Oo][Nn][Tt][Ii][Nn][Uu][Ee])([Dd][Ee][Cc][Ll][Aa][Rr][Ee])([Dd][Ee][Ff][Aa][Uu][Ll][Tt])([Dd][Ii][Ee]\(\))([Dd][Oo])([Ee][Cc][Hh][Oo])([Ee][Ll][Ss][Ee])([Ee][Ll][Ss][Ee][Ii][Ff])([Ee][Mm][Pp][Tt][Yy]\(\))([Ee][Nn][Dd][Dd][Ee][Cc][Ll][Aa][Rr][Ee])([Ee][Nn][Dd][Ff][Oo][Rr])([Ee][Nn][Dd][Ff][Oo][Rr][Ee][Aa][Cc][Hh])([Ee][Nn][Dd][Ii][Ff])([Ee][Nn][Dd][Ss][Ww][Ii][Tt][Cc][Hh])([Ee][Nn][Dd][Ww][Hh][Ii][Ll][Ee])([Ee][Vv][Aa][Ll]\(\))([Ee][Xx][Ii][Tt]\(\)([Ee][Xx][Tt][Ee][Nn][Dd][Ss])([Ff][Ii][Nn][Aa][Ll])([Ff][Ii][Nn][Aa][Ll][Ll][Yy])([Ff][Oo][Rr])([Ff][Oo][Rr][Ee][Aa][Cc][Hh])([Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn])([Gg][Ll][Oo][Bb][Aa][Ll])([Gg][Oo][Tt][Oo])([Ii][Ff])([Ii][Mm][Pp][Ll][Ee][Mm][Ee][Nn][Tt][Ss])([Ii][Nn][Cc][Ll][Uu][Dd][Ee])([Ii][Nn][Cc][Ll][Uu][Dd][Ee]_[Oo][Nn][Cc][Ee])([Ii][Nn][Ss][Tt][Aa][Nn][Cc][Ee][Oo][Ff])([In][Ss][Tt][Ee][Aa][Dd][Oo][Ff])([Ii][Nn][Tt][Ee][Rr][Ff][Aa][Cc][Ee])([Ii][Ss][Ss][Ee][Tt]\(\))[Ll][Ii][Ss][Tt]\(\))([Nn][Aa][Mm][Ee][Ss][Pp][Aa][Cc][Ee])([Nn][Ee][Ww])([Oo][Rr])([Pp][Rr][Ii][Nn][Tt])([Pp][Rr][Ii][Vv][Aa][Tt][Ee])([Pp][Rr][Oo][Tt][Ee][Cc][Tt][Ee][Dd])([Pp][Uu][Bb][Ll][Ii][Cc])([Rr][Ee][Qq][Uu][Ii][Ee][Rr])([Rr][Ee][Qq][Uu][Ii][Ee][Rr]_[Oo][Nn][Cc][Ee])([Rr][Ee][Tt][Uu][Rr][Nn])([Ss][Tt][Aa][Tt][Ii][Cc])([Ss][Ww][Ii][Tt][Cc][Hh])([Tt][Hh][Rr][Oo][Ww])([Tt][Rr][Aa][Ii][Tt])([Tt][Rr][Yy])([Uu][Nn][Ss][Ee][Tt]\(\))([Uu][Ss][Ee])([Vv][Aa][Rr])([Ww][Hh][Ii][Ll][Ee])([Xx][Oo][Rr])([Yy][Ii][Ee][Ll][Dd])]
ifthenelse = ([Ii][Ff]\s*\((\S|\s)*\{(\S|\s)*\}\s*[Ee][Ll][Ss][Ee]\s*\{(\S|\s)*\})
While = ([Ww][Hh][Ii][Ll][Ee])\s*\((\s|\S)+\)\s*\{(\s|\S)+\}
Dowhile = ([Dd][Oo])\s*\{(\s|\S)+\}\s*([Ww][Hh][Ii][Ll][Ee])\s*\((\s|\S)+\);
For = ([Ff][Oo][Rr])\s*\((\s|\S)+\)\s\{(\s|\S)+\}
Foreach = ([Ff][Oo][Rr][Ee][Aa][Cc][Hh])\s*\((\s|\S)+\)\s*\{(\s|\S)+\}
Break = ([Bb][Rr][Ee][Aa][Kk]);
Switch = ([Ss][Ww][Ii][Tt][Cc][Hh])\s*\((\s|\S)+\)\s*\{(\s|\S)+\}
Include = ([Ii][Nn][Cc][Ll][Uu][Dd][Ee])\s*\'(\s|\S)+\';
CamposAcceso = \$([Rr][Ee][Cc][Oo][Rr][Dd][Ee][Rr][Ss][Ee][Tt])\[\'([a-zA-Z_][a-zA-Z0-9]*)\'\]
Funciones = ([Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn])\s*[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*\s*\((\s|\S)+\)\s*\{(\s|\S)+\}

%{
	String File = "";
	String ArchivoEscribir = "";
	List<String> ErroresCol = new ArrayList();
	List<String> ErroresFil = new ArrayList();
	Scanner scanner = new Scanner(System.in);
%}
%eof{
	System.out.println("Inserte nombre de archivo");
	File = scanner.nextLine();
	try{
		PrintWriter writer = new PrintWriter(File + ".out", "UTF-8");
		writer.print(ArchivoEscribir);
		writer.close();
	}catch (Exception e){
		System.err.println("Error: " + e.getMessage());
	}
%eof}
%%
{EtiquetaInicio} 	{
						ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
					}
{PalabrasR} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{OperadoresA} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{OperadoresL} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{Variables} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{VariablesVariables} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{Bool} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{Enteros} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{Reales} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{Cadenas} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{CadenasCDoble} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{Global} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
	}
{Server} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{GET} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{POST} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{FILES} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{COOKIE} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{SESSION} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{REQUEST} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{ENV} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{COOKIE} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{MsjError} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{DatosPost} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{EncabezadosHTTP} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{NumArgScript} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{ArrayArgScript} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{Constatntes} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{Identificador} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{Comentario} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{ComentarioMultiliena} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{ComentarioConsola} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
{PalabrasR} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{ifthenelse} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{While} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{Dowhile} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{For} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{Foreach} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{Break} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{Switch} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{Include} {
	ArchivoEscribir = ArchivoEscribir + yytext().toLowerCase();
}
{CamposAcceso} {
	ArchivoEscribir = ArchivoEscribir + yytext().toUpperCase();
}
{Funciones} {
	ArchivoEscribir = ArchivoEscribir + yytext();
}
. {
	ArchivoEscribir = ArchivoEscribir + "Error" + yytext();
	ErroresCol.add(Integer.toString(yycolumn));
	ErroresFil.add(Integer.toString(yyline));
}